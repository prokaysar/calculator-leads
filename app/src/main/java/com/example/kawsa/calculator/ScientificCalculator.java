package com.example.kawsa.calculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ScientificCalculator extends AppCompatActivity implements View.OnClickListener{
    private EditText input;
    private Button srt,root,qube,sin,cos,tan;
    private TextView result;
    private ScientificCalculatorfunction  calculatorfunction;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scientific_calculator);

        input = findViewById(R.id.getvalue_id);
        srt = findViewById(R.id.squre_id);
        root = findViewById(R.id.root_id);
        qube = findViewById(R.id.qube_id);
        sin = findViewById(R.id.sin_id);
        cos = findViewById(R.id.cos_id);
        tan = findViewById(R.id.tan_id);
        result = findViewById(R.id.result_sin_id);

        srt.setOnClickListener(this);
        root.setOnClickListener(this);
        qube.setOnClickListener(this);
        sin.setOnClickListener(this);
        cos.setOnClickListener(this);
        tan.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        double value = Double.parseDouble(input.getText().toString().trim());
        calculatorfunction = new ScientificCalculatorfunction();
        switch (v.getId()){
            case R.id.squre_id:
                result.setText(calculatorfunction.scquire(value)+"");
                break;
            case R.id.root_id:
                result.setText(calculatorfunction.quireRoot(value)+"");
                break;
            case R.id.qube_id:
                result.setText(calculatorfunction.qubeVale(value)+"");
                break;
            case R.id.sin_id:
                result.setText(calculatorfunction.valueOfSin(value)+"");
                break;
            case R.id.cos_id:
                result.setText(calculatorfunction.valueOfCos(value)+"");
                break;
            case R.id.tan_id:
                result.setText(calculatorfunction.valueOfTan(value)+"");
                break;
        }

    }
}
