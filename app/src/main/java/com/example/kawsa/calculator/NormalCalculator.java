package com.example.kawsa.calculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class NormalCalculator extends AppCompatActivity implements View.OnClickListener{
    private EditText num1,num2;
    private Button add,sub,multi,div;
    private TextView resutt;
    private Calculator calculator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_normal_calculator);

        num1 = findViewById(R.id.num1_id);
        num2 = findViewById(R.id.num2_id);
        add = findViewById(R.id.add_id);
        sub = findViewById(R.id.sub_id);
        multi = findViewById(R.id.multi_id);
        div = findViewById(R.id.div_id);

        resutt = findViewById(R.id.nresult_id);

        add.setOnClickListener(this);
        sub.setOnClickListener(this);
        multi.setOnClickListener(this);
        div.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        float n1 = Float.parseFloat(num1.getText().toString().trim());
        float n2 = Float.parseFloat(num2.getText().toString().trim());
         calculator = new Calculator();

        switch (v.getId()){
            case R.id.add_id:
                resutt.setText(calculator.addition(n1,n2)+"");

                break;
            case R.id.sub_id:

                resutt.setText(calculator.subtraction(n1,n2)+"");
                break;
            case R.id.multi_id:
                resutt.setText(calculator.multiplication(n1,n2)+"");;

                break;
            case R.id.div_id:
                resutt.setText(calculator.division(n1,n2)+"");

                break;
        }

    }
}
