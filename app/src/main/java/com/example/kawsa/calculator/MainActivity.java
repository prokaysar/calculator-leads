package com.example.kawsa.calculator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity  implements View.OnClickListener{
    private Button normaalCalculator,scientificCalculator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        normaalCalculator = findViewById(R.id.button);
        scientificCalculator = findViewById(R.id.button2);
        normaalCalculator.setOnClickListener(this);
        scientificCalculator.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button:
                startActivity(new Intent(MainActivity.this,NormalCalculator.class));
                break;
            case R.id.button2:
                startActivity(new Intent(MainActivity.this,ScientificCalculator.class));
                break;
        }
    }
}
